var WebSocket = require('ws')
var Sound = require('node-aplay')

var ws = new WebSocket('wss://serverseriousgame.herokuapp.com')

ws.on('message', function incoming(data) {
  if (data === 'push' || data === 'fail' || data === 'success') {
		console.log(data)
		new Sound(__dirname + '/sounds/' + data + '.wav').play()
	}
})
